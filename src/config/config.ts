interface Config {
    salt: string;
    validator: {
        validationError: {
            target: boolean;
            value: boolean;
        },
    };
    session: {
        secret: string;
    };

    pusherOptions: {
        appId: string,
        key: string,
        secret: string,
        cluster: string,
        useTLS: boolean,
    };
}

export const config: Config = {
    salt: process.env.APP_SALT,
    validator: {
        validationError: {
            target: false,
            value: false,
        },
    },
    session: {
        secret: process.env.JWT_KEY,
    },

    pusherOptions: {
        appId: '613693',
        key: '1f420441a89d17034c56',
        secret: '28ae3191a367fd354ade',
        cluster: 'eu',
        useTLS: true,
    },
};
