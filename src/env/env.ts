import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export const DATABASE: TypeOrmModuleOptions = {
    type: 'mysql',
    host: '127.0.0.1',
    port: 3306,
    username: 'root',
    password: 'root',
    database: 'node_db',
    entities: [__dirname + '/../**/*.entity{.ts, .js}'],
    synchronize: true,
};
