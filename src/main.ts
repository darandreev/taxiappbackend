import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { useContainer } from 'typeorm';
import * as compression from 'compression';
import * as express from 'express';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  app.use(compression());
  app.use('/images', express.static('images'));
  useContainer(app.select(AppModule), { fallbackOnErrors: true });
  await app.listen(3003);
}
bootstrap();
