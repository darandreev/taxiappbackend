import { BaseEntity, DeepPartial, DeleteResult, QueryFailedError } from 'typeorm';
import { CrudService } from './crud.service';
import { Get, Param, Post, Put, Body, Patch, Delete, Query, HttpException, Res } from '@nestjs/common';

export class RestController<T extends BaseEntity> {

    constructor(public service: CrudService<T>) { }

    @Get('/')
    public async findAll(): Promise<T[]> {
        return this.service.findAll();
    }

    @Get('/:id')
    public async findOne(@Param('id') id: string) {
        return this.service.findOneById(id);
    }

    @Post('/')
    public async create(@Body() data: DeepPartial<T>): Promise<T> {
        try {
            return await this.service.create(data);
        } catch (error) {
            if (error instanceof QueryFailedError) {
                throw new HttpException(error.message, 500);
            } else {
                throw new HttpException({ error }, 500);
            }
        }
    }

    @Put('/:id')
    public async update(@Body() data: DeepPartial<T>,
                        @Query() clientId?: string): Promise<T> {
        return this.service.update(data);
    }

    @Patch('/:id')
    public async patch(@Param('id') id: string, @Body() data: DeepPartial<T>): Promise<T> {
        return this.service.patch(id, data);
    }

    @Delete('/:id')
    public async delete(@Param('id') id: string): Promise<DeleteResult> {
        return this.service.delete(id);
    }
}
