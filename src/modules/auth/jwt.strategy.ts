import {ExtractJwt, Strategy} from 'passport-jwt';
import {PassportStrategy} from '@nestjs/passport';
import {Injectable, UnauthorizedException} from '@nestjs/common';
import { AuthService } from './auth.service';
import { JwtPayload } from './interface/jwtpayload.interface';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
    constructor(private readonly authService: AuthService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: 'pe4enakoko6ka',
            passReqToCallback: true,
        },
        async (req, payload, next) => await this.validate(req, payload, next),
        );
    }

    // tslint:disable-next-line:ban-types
    async validate(request, payload: JwtPayload, done: Function) {
        const user = await this.authService.validateUser(payload);
        if (!user) {
            return done(new UnauthorizedException(), false);
        }
        request.user = user;
        done(null, user);
    }
}
