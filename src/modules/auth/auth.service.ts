import { Injectable, Inject, forwardRef } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import { JwtPayload } from './interface/jwtpayload.interface';
import { UserService } from '../user/user.service';
import { LoginUserDto } from '../user/dto/login-user.dto';

@Injectable()
export class AuthService {
    constructor(@Inject(forwardRef(() => UserService)) private readonly userService: UserService) {}

    async createToken(credentials: LoginUserDto) {
        const user = await this.userService.login(credentials);
        const expiresIn = Math.floor(Date.now() / 1000) + (60 * 60);
        const accessToken = jwt.sign({id: user.id, type: user.type}, 'pe4enakoko6ka', {
            expiresIn,
        });

        return {
            expiresIn,
            accessToken,
            id: user.id,
            driver: user.driver ? user.driver : null,
        };
    }

    async verifyToken(token: string) {
        return new Promise(resolve => {
            token = token.replace(/^Bearer\s/, '');
            jwt.verify(token, 'pe4enakoko6ka', decoded => resolve(decoded));
        });
    }

    async validateUser(payload: JwtPayload): Promise<any> {
        return await this.userService.findOneById(payload.id);
    }
}
