import { Controller, Get, Post, UseGuards, Headers, Body } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthGuard } from '@nestjs/passport';
import { LoginUserDto } from '../user/dto/login-user.dto';

@Controller('auth')
export class AuthController {
    constructor(private authService: AuthService) {}

    @Get('verify')
    @UseGuards(AuthGuard('jwt'))
    public async verify(@Headers('authorization') token: string) {
        return this.authService.verifyToken(token);
    }

    @Post('token')
    public async getToken(@Body() credentials: LoginUserDto) {
        return await this.authService.createToken(credentials);
    }
}
