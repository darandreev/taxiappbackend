import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { UserService } from '../user/user.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from '../user/user.entity';
import { Repository } from 'typeorm';
import { LoginUserDto } from '../user/dto/login-user.dto';

describe('Auth Controller', () => {
  let controller: AuthController;
  let authService: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        UserService,
        {provide: getRepositoryToken(User), useClass: Repository},
        {
          provide: AuthService,
          useFactory: () => ({
            createToken: jest.fn(() => true),
          }),
        },
      ],
      controllers: [AuthController],
    }).compile();

    controller = module.get<AuthController>(AuthController);
    authService = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getToken and login', () => {
    it('should create and return token', async () => {
      const credentials: LoginUserDto = {
        username: 'darko',
        password: 'darko123',
        type: 'client',
      };

      controller.getToken(credentials);
      expect(authService.createToken).toBeCalledWith(credentials);
    });
  });

});
