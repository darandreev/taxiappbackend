import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, FindConditions } from 'typeorm';
import { TaxiOrder, OrderStatus } from './taxiorder.entity';
import * as distance from 'google-distance-matrix';
import { CrudService } from '../../base/crud.service';

const capitalize = (s: string) => {
    return s.charAt(0).toUpperCase() + s.slice(1);
};

const options = (orderStatus: string, userId: string): FindConditions<TaxiOrder> => {
    if (orderStatus === OrderStatus.Pending) {
        return { status: OrderStatus[orderStatus] };
    } else {
        return { userId, status: OrderStatus[orderStatus] };
    }
};

@Injectable()
export class TaxiorderService extends CrudService<TaxiOrder> {

    constructor(
        @InjectRepository(TaxiOrder)
        private taxiOrderRepository: Repository<TaxiOrder>) {
            super(taxiOrderRepository);
        }

    async getAllByStatus(status: string, id?: string): Promise<TaxiOrder[]> {
        status = capitalize(status);
        if (!OrderStatus[status]) {
            throw new NotFoundException('Order status not found');
        }
        const orders = await this.taxiOrderRepository.find(options(status, id));

        if (!orders.length) {
            throw new NotFoundException('No orders for this status');
        }

        return orders;
    }

    public getDistanceTime(addressFrom: string[], addressTo: string[]): Promise<any> {
        distance.key('AIzaSyCFLqp7dGAeehxj73Ptd5TbTVDRxeIVybU');
        distance.mode('driving');
        const promise = new Promise((resolve, reject) => {
            distance.matrix(addressFrom, addressTo, (err, distances) => {
                if (!err) {
                    if (distances.status === 'OK') {
                        if (distances.rows[0].elements[0].status === 'OK') {
                            const duration = distances.rows[0].elements[0].duration.value;
                            resolve(duration);
                        }
                    }
                } else {
                    reject(err);
                }
            });
        });

        return promise;
    }
}
