import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, BaseEntity } from 'typeorm';
import { User } from '../user/user.entity';

export enum OrderStatus {
    Pending = 'Pending',
    Accepted = 'Accepted',
    Canceled = 'Canceled',
    Completed = 'Completed',
    Arrived = 'Arrived',
}

@Entity()
export class TaxiOrder extends BaseEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ type: 'double' })
    latitudeFrom: number;

    @Column({ type: 'double' })
    longitudeFrom: number;

    @Column({ type: 'double' })
    latitudeTo: number;

    @Column({ type: 'double' })
    longitudeTo: number;

    @Column({ type: 'varchar' })
    addressFrom: string;

    @Column({ type: 'varchar' })
    addressTo: string;

    @Column({ type: 'double' })
    price: number;

    @Column({ type: 'enum', enum: OrderStatus, default: OrderStatus.Pending })
    status: OrderStatus;

    @Column()
    userId?: string;

    @ManyToOne(type => User)
    @JoinColumn({ name: 'userId', referencedColumnName: 'id' })
    user?: User;
}
