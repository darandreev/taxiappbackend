import { Module } from '@nestjs/common';
import { TaxiorderController } from './taxiorder.controller';
import { TaxiorderService } from './taxiorder.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TaxiOrder } from './taxiorder.entity';
import { UserModule } from '../user/user.module';

@Module({
  imports: [TypeOrmModule.forFeature([TaxiOrder]), UserModule],
  controllers: [TaxiorderController],
  providers: [TaxiorderService],
})
export class TaxiorderModule {}
