import { Controller, Post, Body, Put, Query, Get, HttpCode, HttpStatus } from '@nestjs/common';
import { TaxiorderService } from './taxiorder.service';
import { TaxiOrder, OrderStatus } from './taxiorder.entity';
import * as Pusher from 'pusher';
import { UserService } from '../user/user.service';
import { RestController } from '../../base/rest.controller';
import { config } from '../../config/config';
import { AuthGuard } from '@nestjs/passport';

@Controller('taxiorder')
export class TaxiorderController extends RestController<TaxiOrder> {
    private pusher: Pusher;

    constructor(private taxiOrderService: TaxiorderService,
                private userService: UserService) {
                    super(taxiOrderService);
                    this.pusher = new Pusher(config.pusherOptions);
                }

    @Post()
    async create(@Body() taxiOrder: TaxiOrder) {
        if (!taxiOrder || (taxiOrder && Object.keys(taxiOrder).length === 0)) {
            throw new Error('Unable to create order!');
        }

        const result = await this.taxiOrderService.create(taxiOrder);

        if (result) {
            const user = await this.userService.findOne({
                id: taxiOrder.userId,
            }, { relations: ['client']});
            delete user.password;

            this.pusher.trigger('taxiorder', 'taxiorder_data', { taxiOrder, user });
        }

        return result;
    }

    @Put('/update')
    @HttpCode(HttpStatus.OK)
    async update(
            @Body() taxiOrder: TaxiOrder,
            @Query('clientId') clientId?: string,
            @Query('driverLocation') driverLocation?: string,
            ) {

        const user = await this.userService.getById(taxiOrder.userId);
        const result = await this.taxiOrderService.update(taxiOrder);

        let duration: string;
        if (taxiOrder.status === OrderStatus.Accepted) {
            duration = await this.taxiOrderService.getDistanceTime([driverLocation], [taxiOrder.addressFrom]);
        }

        if (result && user) {
            this.pusher.trigger('taxiorder', 'taxiorder_data', { taxiOrder, user, clientId, duration });
        }

        return result;
    }

    @Post('/location')
    async sendLocation(@Body() location) {
        this.pusher.trigger('taxiorder', 'taxiorder_location', location);
    }

    @Post('/message')
    sendMessage(@Body() message) {
        this.pusher.trigger('taxiorder', 'taxi_message', message);
    }

    @Get('status')
    async getByStatus(
        @Query('orderStatus') orderStatus: string,
        @Query('userId') userId?: string,
        ) {
        return await this.taxiOrderService.getAllByStatus(orderStatus, userId);
    }
}
