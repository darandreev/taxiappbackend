import { Test, TestingModule } from '@nestjs/testing';
import { TaxiorderService } from './taxiorder.service';
import { TaxiOrder, OrderStatus } from './taxiorder.entity';
import { Repository } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';

describe('TaxiorderService', () => {
  let service: TaxiorderService;
  let userRepository: any;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TaxiorderService,
        {
          provide: getRepositoryToken(TaxiOrder),
          useClass: Repository,
          useFactory: () => ({
            find: jest.fn(() => true),
          }),
        },
      ],
    }).compile();

    service = module.get<TaxiorderService>(TaxiorderService);
    userRepository = module.get(getRepositoryToken(TaxiOrder));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getAllByStatus', async () => {
    const orders: TaxiOrder[] = [
      {
        id: '123123',
        latitudeFrom: 56.76,
        longitudeFrom: 12,
        latitudeTo: 15,
        longitudeTo: 19,
        addressFrom: 'adres',
        addressTo: 'adres',
        price: 18,
        status: OrderStatus.Accepted,
        hasId: () => true,
        save: () => null,
        reload: () => null,
        remove: () => null,
      },
    ];
    expect(userRepository.find = jest.fn(() => orders)).not.toBeNull();
  });
});
