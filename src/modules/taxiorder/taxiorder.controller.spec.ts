import { Test, TestingModule } from '@nestjs/testing';
import { TaxiorderController } from './taxiorder.controller';
import { TaxiorderService } from './taxiorder.service';
import { Repository } from 'typeorm';
import { TaxiOrder, OrderStatus } from './taxiorder.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UserService } from '../user/user.service';
import { User } from '../user/user.entity';

describe('Taxiorder Controller', () => {
  let controller: TaxiorderController;
  let taxiOrderService: TaxiorderService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserService,
        { provide: getRepositoryToken(TaxiOrder), useClass: Repository },
        { provide: getRepositoryToken(User), useClass: Repository },
        {
          provide: TaxiorderService,
          useFactory: () => ({
            getAllByStatus: jest.fn(() => true),
            create: jest.fn(() => true),
          }),
        },
      ],
      controllers: [TaxiorderController],
    }).compile();

    controller = module.get<TaxiorderController>(TaxiorderController);
    taxiOrderService = module.get<TaxiorderService>(TaxiorderService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getByStatus', () => {
    it ('should call taxiOrderService method getAllByStatus', async () => {
      const params = {
        orderStatus: 'Pending',
        userId: '12jjadajsd8',
      };
      controller.getByStatus(params.orderStatus, params.userId);
      expect(taxiOrderService.getAllByStatus).toHaveBeenCalled();
    });

    it ('should call taxiOrderService method getAllByStatus', async () => {
      const params = {
        orderStatus: 'Pending',
        userId: '12jjadajsd8',
      };

      const orders: TaxiOrder[] = [
        {
          id: '123123',
          latitudeFrom: 56.76,
          longitudeFrom: 12,
          latitudeTo: 15,
          longitudeTo: 19,
          addressFrom: 'adres',
          addressTo: 'adres',
          price: 18,
          status: OrderStatus.Pending,
          hasId: () => true,
          save: () => null,
          reload: () => null,
          remove: () => null,
        },
      ];
      jest.spyOn(taxiOrderService, 'getAllByStatus').mockImplementation(async () => await orders);
      expect(await controller.getByStatus(params.orderStatus, params.userId)).toBe(orders);
    });

  });

  describe('createTaxiOrder', () => {
    it('should create new order', async () => {
      const order: TaxiOrder = {
          id: '123123',
          latitudeFrom: 56.76,
          longitudeFrom: 12,
          latitudeTo: 15,
          longitudeTo: 19,
          addressFrom: 'adres',
          addressTo: 'adres',
          price: 18,
          status: OrderStatus.Pending,
          hasId: () => true,
          save: () => null,
          reload: () => null,
          remove: () => null,
      };

      controller.create(order);
      expect(taxiOrderService.create).toHaveBeenCalledWith(order);
    });
  });
});
