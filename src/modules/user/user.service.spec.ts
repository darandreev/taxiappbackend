import { Test, TestingModule } from '@nestjs/testing';
import { UserService } from './user.service';
import { DriverService } from './driver/driver.service';
import { UserController } from './user.controller';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from './user.entity';
import { Repository } from 'typeorm';
import { Driver } from './driver/driver.entity';

describe('UserService', () => {
  let service: UserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserService,
        DriverService,
        {provide: getRepositoryToken(User), useClass: Repository},
        {provide: getRepositoryToken(Driver), useClass: Repository},
      ],
      controllers: [UserController],
    }).compile();

    service = module.get<UserService>(UserService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
