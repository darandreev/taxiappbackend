import { Controller, Get } from '@nestjs/common';
import { UserService } from './user.service';
import { User } from './user.entity';
import { RestController } from '../../base/rest.controller';

@Controller('user')
export class UserController extends RestController<User> {

    constructor(public userService: UserService) {
        super(userService);
    }

    @Get()
    async getAll(): Promise<User[]> {
        return await this.userService.getAll();
    }
}
