import { Entity, Column, PrimaryGeneratedColumn, BeforeInsert, OneToMany, BaseEntity, OneToOne, JoinColumn } from 'typeorm';
import { IsEmail } from 'class-validator';
import { Exclude } from 'class-transformer';
import { Driver } from './driver/driver.entity';
import { Client } from './client/client.entity';
import { TaxiOrder } from '../taxiorder/taxiorder.entity';
import { passwordHash } from '../../helpers/password.hash';

@Entity('user')
export class User extends BaseEntity {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ type: 'varchar', length: 255, nullable: false, unique: true })
    username: string;

    @Column({ type: 'varchar', length: 255, nullable: false, unique: true })
    @IsEmail()
    email: string;

    @Column({ type: 'varchar', length: 255, nullable: false })
    type: string;

    @Column()
    @Exclude({ toPlainOnly: true })
    password: string;

    @OneToMany(type => TaxiOrder, order => order.user)
    taxiOrder: TaxiOrder[];

    @OneToOne(type => Driver, driver => driver.user, { cascade: true, nullable: true, onUpdate: 'CASCADE', onDelete: 'CASCADE' })
    @JoinColumn()
    driver?: Driver;

    @OneToOne(type => Client, client => client.user, { cascade: true, nullable: true, onUpdate: 'CASCADE', onDelete: 'CASCADE' })
    @JoinColumn()
    client?: Client;

    @BeforeInsert()
    hashPassword() {
        this.password = passwordHash(this.password);
    }

}
