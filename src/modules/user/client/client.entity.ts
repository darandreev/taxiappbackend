import { Entity, PrimaryGeneratedColumn, Column, OneToOne } from 'typeorm';
import { User } from '../user.entity';

@Entity()
export class Client {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ type: 'varchar', length: 255, nullable: true })
    fullName: string;

    @Column({ type: 'varchar', length: 255, nullable: true })
    phoneNumber: string;

    @OneToOne(type => User, user => user.driver)
    user: User;
}
