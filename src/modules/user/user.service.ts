import { Injectable, NotFoundException, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './user.entity';
import { Repository } from 'typeorm';
import { LoginUserDto } from './dto/login-user.dto';
import { CrudService } from '../../base/crud.service';
import { passwordHash } from '../../helpers/password.hash';

@Injectable()
export class UserService extends CrudService<User> {

    constructor(@InjectRepository(User) protected readonly userRepository: Repository<User>) {
        super(userRepository);
    }

    public async login(credentials: LoginUserDto) {
        const user = await this.userRepository.findOne({
            username: credentials.username,
            password: passwordHash(credentials.password),
        }, { relations: ['driver', 'client']});

        if (!user) {
            throw new NotFoundException('User not found');
        }

        if (user.driver && !user.driver.approved) {
            throw new HttpException('Driver user not approved', 500);
        }

        return user;
    }

    public async getById(id: string): Promise<User> {
        return await this.repository.findOneOrFail({
            id,
        }, { relations: ['driver', 'client'] });
    }

    public async getAll(): Promise<User[]> {
        const users = await User.createQueryBuilder('user')
            .leftJoinAndSelect('user.driver', 'driver')
            .where('driver.approved = :approved', { approved: false })
            .getMany();

        users.map(u => delete u.password);

        return users;
    }
}
