import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './user.entity';
import { Driver } from './driver/driver.entity';
import { DriverService } from './driver/driver.service';
import { DriverModule } from './driver/driver.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Driver]),
    DriverModule,
  ],
  providers: [UserService, DriverService],
  controllers: [UserController],
  exports: [UserService],
})
export class UserModule {}
