import { Entity, PrimaryGeneratedColumn, Column, OneToOne, BaseEntity } from 'typeorm';
import { User } from '../user.entity';

@Entity()
export class Driver extends BaseEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ type: 'varchar', length: 255, nullable: false })
    fullName: string;

    @Column({ type: 'varchar', length: 255, nullable: false })
    phoneNumber: string;

    @Column({ type: 'varchar', length: 255, nullable: false })
    carColor: string;

    @Column({ type: 'varchar', length: 255, nullable: false })
    carNumber: string;

    @Column({ type: 'varchar', length: 255, nullable: false })
    carModel: string;

    @Column({ type: 'varchar', length: 255, nullable: false })
    carImage: string;

    @Column({ type: 'varchar', length: 255, nullable: false })
    driverImage: string;

    @Column({ type: 'boolean', default: false })
    available: boolean;

    @Column({ type: 'boolean', default: false })
    approved: boolean;

    @OneToOne(type => User, user => user.driver)
    user: User;
}
