import { Injectable, HttpException } from '@nestjs/common';
import { Driver } from './driver.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, DeepPartial } from 'typeorm';
import { CrudService } from '../../../base/crud.service';

export interface DriverAvailable {
    available: boolean;
}

export interface DriverApproved {
    approved: boolean;
}

@Injectable()
export class DriverService extends CrudService<Driver> {
    constructor(@InjectRepository(Driver) protected readonly repository: Repository<Driver>) {
        super(repository);
    }

    async updateAvailable(driver: DeepPartial<Driver>): Promise<DriverAvailable> {
        const result = await this.repository.update(driver.id, {
            available: driver.available,
        });

        if (!result) {
            throw new HttpException('Error!', 500);
        }

        return {
            available: driver.available,
        };
    }

    async findAll(): Promise<Driver[]> {
        return await this.repository.find({
            relations: ['driver', 'client'],
        });
    }

    async updateDriverStatus(driver: DeepPartial<Driver>): Promise<DriverApproved> {
        const result = this.repository.update(driver.id, {
            approved: driver.approved,
        });

        if (!result) {
            throw new HttpException('Error!', 500);
        }

        return {
            approved: driver.approved,
        };
    }
}
