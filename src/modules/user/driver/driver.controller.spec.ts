import { Test, TestingModule } from '@nestjs/testing';
import { DriverController } from './driver.controller';
import { RestController } from '../../../base/rest.controller';
import { DriverService } from './driver.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Driver } from './driver.entity';
import { Repository } from 'typeorm';

describe('Driver Controller', () => {
  let controller: DriverController;
  let driverService: DriverService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DriverService,
        {provide: getRepositoryToken(Driver), useClass: Repository},

      ],
      controllers: [DriverController, RestController],
    }).compile();

    controller = module.get<DriverController>(DriverController);
    driverService = module.get<DriverService>(DriverService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
