import { Controller, Put, Body, Post, UseInterceptors, FileInterceptor, UploadedFile, Res, HttpCode, HttpException, Req, Get } from '@nestjs/common';
import { DriverService } from './driver.service';
import { Driver } from './driver.entity';
import { DeepPartial } from 'typeorm';
import { RestController } from '../../../base/rest.controller';
import * as multer from 'multer';
import { Response, Request } from 'express';

const storage = multer.diskStorage({
    destination(req, file, cb) {
        cb(null, './images');
    },
    filename(req, file, cb) {
        cb(null, file.originalname);
    },
});

@Controller('driver')
export class DriverController extends RestController<Driver> {
    constructor(public driverService: DriverService) {
        super(driverService);
    }

    @Put('/available')
    async updateDriverAvailability(@Body() driver: DeepPartial<Driver>) {
        return await this.driverService.updateAvailable(driver);
    }

    @Put('/approve')
    async updateDriverStatus(@Body() driver: DeepPartial<Driver>) {
        return await this.driverService.updateDriverStatus(driver);
    }

    @Post('/image')
    @HttpCode(400)
    @UseInterceptors(FileInterceptor('file', { storage }))
    uploadImage(@UploadedFile() image, @Res() response: Response, @Req() req: Request) {
        try {
            if (!image) {
                throw new HttpException({ error: 'No image provided!' }, 500);
            }
            image.type = req.body.type;
            return response.status(200).json(image);
        } catch (error) {
            throw new HttpException(error, 500);
        }
    }

    @Get()
    async getAll(): Promise<Driver[]> {
        return await this.driverService.findAll();
    }
}
